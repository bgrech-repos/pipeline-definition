#!/bin/bash

set -Eeuo pipefail
# no shopt -s inherit_errexit as this needs to run on the ancient Bash in RHEL7

cd "$(dirname "${BASH_SOURCE[0]}")"

tests/functions.sh
tests/aws_functions.sh
tests/prepare.sh
tests/artifacts.sh
tests/merge.sh
