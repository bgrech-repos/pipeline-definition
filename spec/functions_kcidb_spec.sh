#!/bin/bash
# shellcheck disable=SC2312         # masking of return values

eval "$(shellspec - -c) exit 1"

Describe 'default setting in test plan'
    # The tested function uses sponge which is not available on our builders,
    # where this function should not be executed anyways.
    is_not_fedora() { grep -qv Fedora /etc/redhat-release ; }
    Skip if "is not Fedora" is_not_fedora

    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    KCIDB_DUMPFILE_NAME=kcidb_all.json
    export DEBUG_KERNEL
    DEBUG_KERNEL="true"

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    Context 'if there are tests in the KCIDB file'
        It "correctly sets the values"
            cp tests/assets/stub_kcidb.json "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_set_test_defaults
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"debug": true'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"targeted": false'
        End
    End

    Context 'if there are no tests but empty tests list entry is present'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests[])' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_set_test_defaults
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"tests": []'
        End
    End

    Context 'if there is no test entry'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests)' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_set_test_defaults
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"tests"'
        End
    End
End

Describe 'kcidb checkout creation'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    export CKI_DEPLOYMENT_ENVIRONMENT
    export CI_PIPELINE_ID=987654

    Parameters
        production 'brew_task_id=123 web_url=https://dummy.url/koji' "redhat:koji-123" ""
        production 'brew_task_id=123 web_url=https://dummy.url/brew' "redhat:brew-123" ""
        production 'copr_build=123' "redhat:copr-123" ""
        retrigger 'brew_task_id=123 web_url=https://dummy.url/koji' "redhat:987654" ""
        retrigger 'brew_task_id=123 web_url=https://dummy.url/brew' "redhat:987654" ""
        retrigger 'brew_task_id=123 web_url=https://dummy.url/koji' "redhat:koji-123" "original/pipelines/987"
        retrigger 'copr_build=123' "redhat:987654" ""
        retrigger 'git_url=something' "redhat:987" "original/pipeline/987"
        production 'git_url=something' "redhat:987654" ""

    End

    It "correctly creates checkout ID for ${1} ${2} (${4})"
        CKI_DEPLOYMENT_ENVIRONMENT="${1}"
        for variable in ${2} ; do
            export "${variable?}"
        done

        When call create_checkout_id "${4}"
        The output should equal "${3}"
    End
End

Describe 'skipping all tests'
    # The tested function uses ? functionality in jq which is not available on RHEL6,
    # where this function should not be executed anyways.
    is_not_fedora() { grep -qv Fedora /etc/redhat-release ; }
    Skip if "is not Fedora" is_not_fedora

    Include tests/source_functions.sh
    Include tests/helpers.sh

    KCIDB_DUMPFILE_NAME=kcidb_all.json

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        kcidb_calls_file="${CI_PROJECT_DIR}/kcidb_calls"
        touch "${kcidb_calls_file}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    kcidb() {
        # Only save test ID, key, value
        echo "${2} ${4} ${5}" >> "${kcidb_calls_file}"
    }

    Context 'if there are tests in the KCIDB file'
        It "correctly sets the SKIP status"
            # remove the status attribute from the tests in the stub file for this test
            jq 'del(.tests[].status)' ./tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_skip_all_tests
            The status should be success
            The contents of file "${kcidb_calls_file}" should include "redhat:1 status SKIP"
            The contents of file "${kcidb_calls_file}" should include "redhat:2 status SKIP"
            The contents of file "${kcidb_calls_file}" should include "redhat:3 status SKIP"
        End
    End

    Context 'if there are no tests in the KCIDB file'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests)' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_skip_all_tests
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"tests"'
        End
    End
End

Describe 'skipping missing tests'
    # The tested function uses ? functionality in jq which is not available on RHEL6,
    # where this function should not be executed anyways.
    is_not_fedora() { grep -qv Fedora /etc/redhat-release ; }
    Skip if "is not Fedora" is_not_fedora

    Include tests/source_functions.sh
    Include tests/helpers.sh

    KCIDB_DUMPFILE_NAME=kcidb_all.json
    export DEBUG_KERNEL
    DEBUG_KERNEL="true"

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        kcidb_calls_file="${CI_PROJECT_DIR}/kcidb_calls"
        touch "${kcidb_calls_file}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    kcidb() {
        # Only save test ID, key, value
        echo "${2} ${4} ${5}" >> "${kcidb_calls_file}"
    }

    Context 'if there are tests in the KCIDB file'
        It "correctly sets the values"
            cp tests/assets/stub_kcidb.json "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_skip_missing_tests
            The status should be success
            The output should not include "redhat:1: Status is null, setting to SKIP"
            The output should include "redhat:2: Status is null, setting to SKIP"
            The output should include "redhat:3: Status is null, setting to SKIP"
            The contents of file "${kcidb_calls_file}" should not include "redhat:1 status SKIP"
            The contents of file "${kcidb_calls_file}" should include "redhat:2 status SKIP"
            The contents of file "${kcidb_calls_file}" should include "redhat:3 status SKIP"
        End
    End

    Context 'if there are no tests in the KCIDB file'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests)' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_skip_missing_tests
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"tests"'
        End
    End
End

